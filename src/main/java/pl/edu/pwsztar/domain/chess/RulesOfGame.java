package pl.edu.pwsztar.domain.chess;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

public interface RulesOfGame {

    /**
     * Metoda zwraca true, tylko gdy przejscie z polozenia
     * (xStart, yStart) na (xEnd, yEnd) w jednym ruchu jest zgodne
     * z zasadami gry w szachy
     */
    boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd);

    @Component
    @Qualifier("Bishop")
    class Bishop implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (xStart == xEnd && yStart == yEnd) {
                return false;
            }

            return Math.abs(xEnd - xStart) == Math.abs(yEnd - yStart);
        }
    }

    @Component
    @Qualifier("Knight")
    class Knight implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            boolean isFirstVariantCorrect = Math.abs(xStart - xEnd) == 1 && Math.abs(yStart - yEnd) == 2;
            boolean isSecondVariantCorrect = Math.abs(xStart - xEnd) == 2 && Math.abs(yStart - yEnd) == 1;
            return isFirstVariantCorrect ^ isSecondVariantCorrect;
        }
    }

    @Component
    @Qualifier("King")
    class King implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (xStart == xEnd && yStart == yEnd) {
                return false;
            }
            int xMoveLength = Math.abs(xStart - xEnd);
            int yMoveLength = Math.abs(yStart - yEnd);
            return (xMoveLength == 0 || xMoveLength == 1) && (yMoveLength == 0 || yMoveLength == 1);
        }
    }

    @Component
    @Qualifier("Queen")
    class Queen implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (xStart == xEnd && yStart == yEnd) {
                return false;
            }
            int xMoveLength = Math.abs(xStart - xEnd);
            int yMoveLength = Math.abs(yStart - yEnd);
            boolean isBishopMoveCorrect = xMoveLength == yMoveLength;
            boolean isRookMoveCorrect = xMoveLength == 0 || yMoveLength == 0;
            return isBishopMoveCorrect || isRookMoveCorrect;
        }
    }

    @Component
    @Qualifier("Rook")
    class Rook implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (xStart == xEnd && yStart == yEnd) {
                return false;
            }
            int xMoveLength = Math.abs(xStart - xEnd);
            int yMoveLength = Math.abs(yStart - yEnd);
            return xMoveLength == 0 || yMoveLength == 0;
        }
    }

    @Component
    @Qualifier("Pawn")
    class Pawn implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            return xStart == xEnd && yStart == yEnd - 1;
        }
    }
}
